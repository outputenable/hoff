/*
   Copyright (C) 2012, 2013, 2014, 2015 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#ifndef IHEX_IHEXRD_H_INCLUDED
#define IHEX_IHEXRD_H_INCLUDED

#include <stddef.h>

#include <hoff/hoffdef.h>
#include <hoff/ihexdef.h>

#define IHEX_RD_F_NOCASE  HOFF_F_NOCASE

typedef struct ihex_reader_ *ihex_reader;
/*
 * The reader provides (comparatively speaking) low-level facilities for
 * incrementally reading/decoding Intel HEX streams.  Dealing with text,
 * the stream is viewed as a null-terminated string.  As far as the
 * reader is concerned, it consists of a pointer to the next input
 * character (or NUL for end-of-string); possible underflow as well as
 * context have to be managed by the client.  The reader scans for
 * records, handles their syntax, makes available the header fields and
 * updates the (partial) sum.  It does not, however, understand or check
 * any of the "semantic" properties like the format and meaning of the
 * various record types or even the checksum.
 */

extern HOFF_API ihex_reader ihex_rd_new(void);
/*
 * Allocates and initializes a new Intel HEX reader.
 *
 * Returns a pointer to a newly-created reader, or NULL if there's not
 * enough space.  A reader created by ihex_rd_new() must be disposed of
 * with ihex_rd_free().
 *
 * A newly-created reader has no current or next record in progress (that
 * is ihex_rd_getrectyp() < 0) and generally behaves as if
 * ihex_rd_terminate() had just been invoked.
 */

extern HOFF_API void ihex_rd_free(ihex_reader rd);
/*
 * Destroys then frees an Intel HEX reader previously created by
 * ihex_rd_new().
 *
 * rd may be NULL (no-op then).
 */

extern HOFF_API unsigned int ihex_rd_getflags(ihex_reader rd);
extern HOFF_API unsigned int ihex_rd_setflags(ihex_reader rd,
    unsigned int which, unsigned int value);

extern HOFF_API ihex_skipfunc ihex_rd_getskipfunc(ihex_reader rd);
/*
 * Returns the skip function of an Intel HEX reader.
 *
 * See ihex_rd_setskipfunc() for details.
 */

extern HOFF_API void ihex_rd_setskipfunc(ihex_reader rd, ihex_skipfunc skip);
/*
 * Sets the skip function for an Intel HEX reader.
 *
 * The skip function, given a pointer to the input stream and a context,
 * shall skip ahead past the record mark ':' to the start of the next
 * record to read.  In case of success, it shall return 0 and the stream
 * pointer *sptr shall point to the first (assumed) hexadecimal digit of
 * the record (that is the one encoding the high nibble of RECLEN).  Any
 * non-zero return value indicates failure, in which case *sptr need not
 * be defined.
 *
 * If skip is NULL, the default skip function is used.  It does not use
 * context and simply skips any characters up to and including the next
 * record mark, or fails with -IHEX_UNDERFLOW.
 *
 * The skip function is invoked by ihex_rd_next().
 */

extern HOFF_API void *ihex_rd_getcontext(ihex_reader rd);
extern HOFF_API void ihex_rd_setcontext(ihex_reader rd, void *ctx);

extern HOFF_API int ihex_rd_next(ihex_reader rd, const char **sptr);
/*
 * Incrementally loads the next record header from the C string pointed
 * to by *sptr.
 *
 * *sptr is updated (that is advanced) accordingly.
 *
 * The current record, if any, is terminated (via ihex_rd_terminate()).
 * Then the skip function (see ihex_rd_setskipfunc()) is invoked with
 * sptr and ihex_rd_getcontext() to find the start of the next record.
 * If it succeeds, the subsequent header is loaded.  Once completed
 * (possibly incrementally), the value of the now current record's
 * RECTYP header field is returned (which is always >= 0 and also
 * accessible through ihex_rd_getrectyp()).  Otherwise, a non-zero(!)
 * error code is returned at any stage:
 *
 * -IHEX_UNDERFLOW
 *     Stream buffer underflow.  Implies **sptr == '\0'.
 *
 * -IHEX_SYNTAX_ERROR
 *     *sptr points to an illegal input character.
 *
 * Any non-zero(!) return code from the skip function is simply passed
 * on to the caller.
 *
 * The (partial) sum is reset and then updated on the fly.
 */

extern HOFF_API void ihex_rd_terminate(ihex_reader rd);
/*
 * Terminates the current or next record in progress, if any, and
 * prepares the reader for loading an all-new record.
 *
 * This invalidates all header fields and resets the reader's internal
 * state as well as the (partial) sum.  In particular,
 * ihex_rd_getrectyp() < 0 until the next record header has been loaded
 * via ihex_rd_next().
 */

extern HOFF_API int ihex_rd_getrectyp(ihex_reader rd);
/*
 * Returns the value of the RECTYP header field of the current record, or
 * a value less than zero if there is no current record.  In the latter
 * case other attributes of the reader may be undefined.
 */

extern HOFF_API unsigned int ihex_rd_getreclen(ihex_reader rd);
/*
 * Returns the value of the RECLEN header field of the current record.
 *
 * Invoking this function without a current record being available (that
 * is when ihex_rd_getrectyp() < 0) yields undefined behavior.
 */

extern HOFF_API unsigned int ihex_rd_getloadoffs(ihex_reader rd);
/*
 * Returns the value of the LOAD OFFSET header field of the current
 * record.
 *
 * Invoking this function without a current record being available (that
 * is when ihex_rd_getrectyp() < 0) yields undefined behavior.
 */

extern HOFF_API unsigned int ihex_rd_getsum(ihex_reader rd);
/*
 * Returns the (partial) sum of bytes of the current record modulo 0xff.
 *
 * When a complete record including DATA/INFO and CHKSUM has been read
 * (via ihex_rd_next() and ihex_rd_read()), the sum should be 0.
 *
 * Invoking this function without a current record being available (that
 * is when ihex_rd_getrectyp() < 0) yields undefined behavior.
 */

extern HOFF_API size_t ihex_rd_read(void *buf, size_t n, const char **sptr,
    ihex_reader rd);
/*
 * Incrementally reads/decodes a sequence of up to n pairs of hexadecimal
 * digits from the C string pointed to by *sptr to bytes in buf.
 *
 * *sptr is updated (that is advanced) accordingly.
 *
 * Returns the number of complete octets (that is pairs of hexadecimal
 * digits) decoded.  The return value is always less than or equal to n.
 * If the return value is less than n, **sptr determines the error
 * condition:
 *
 * **sptr == '\0'
 *     Stream buffer underflow.
 *
 * otherwise
 *     *sptr points to an illegal input character.
 *
 * The (partial) sum is updated on the fly.
 *
 * Note that the record mark ':' is illegal here.  Note also that the
 * number of hexadecimal /digits/ decoded may differ from the number of
 * characters consumed from *sptr.
 */

#endif /* IHEX_IHEXRD_H_INCLUDED */
