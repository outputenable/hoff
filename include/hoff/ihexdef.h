/*
   Copyright (C) 2012 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#ifndef IHEX_IHEXDEF_H_INCLUDED
#define IHEX_IHEXDEF_H_INCLUDED

#define IHEX_TYP_DAT  0x00  /* Data Record (8-, 16-, or 32-bit formats) */
#define IHEX_TYP_EOF  0x01  /* End of File Record (8-, 16-, or 32-bit formats) */
#define IHEX_TYP_ESA  0x02  /* Extended Segment Address Record (16- or 32-bit formats) */
#define IHEX_TYP_SSA  0x03  /* Start Segment Address Record (16- or 32-bit formats) */
#define IHEX_TYP_ELA  0x04  /* Extended Linear Address Record (32-bit format only) */
#define IHEX_TYP_SLA  0x05  /* Start Linear Address Record (32-bit format only) */

#define IHEX_OK            0
#define IHEX_UNDERFLOW     1
#define IHEX_SYNTAX_ERROR  2
#define IHEX_BAD_CHECKSUM  3
#define IHEX_FORMAT_ERROR  4

#define IHEX_RECORD_MARK  '\x3A'  /* ASCII colon (':') character */

#define IHEX_HDRLEN  4

#define IHEX_RECLEN_MAX  0xff

#define IHEX_SEGMENTED_ADDR_MAX  0xfffffUL
#define IHEX_LINEAR_ADDR_MAX  0xffffffffUL
#define IHEX_ADDR_MAX  IHEX_LINEAR_ADDR_MAX

typedef int (*ihex_skipfunc)(const char **sptr, void *ctx);

#endif /* IHEX_IHEXDEF_H_INCLUDED */
