/*
   Copyright (C) 2012, 2013, 2014, 2015 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#ifndef IHEX_IHEXPS_H_INCLUDED
#define IHEX_IHEXPS_H_INCLUDED

#include <stddef.h>

#include <hoff/hoffdef.h>
#include <hoff/ihexdef.h>

#define IHEX_PS_F_SEGMENTED  (1U << 0)
#define IHEX_PS_F_NOCASE     HOFF_F_NOCASE

typedef struct ihex_parser_ *ihex_parser;
/*
 * The parser extends the reader and adds state and functions to
 * conveniently read and validate Intel HEX streams, dealing with
 * complete records (including DATA/INFO and CHKSUM) at a time.  It also
 * keeps track of the absolute address.  Furthermore, it allows for
 * reading contiguous blocks of data /across/ records without having to
 * deal with each record individually.
 */

extern HOFF_API ihex_parser ihex_ps_new(void);
extern HOFF_API void ihex_ps_free(ihex_parser ps);

extern HOFF_API ihex_skipfunc ihex_ps_getskipfunc(ihex_parser ps);
extern HOFF_API void ihex_ps_setskipfunc(ihex_parser ps, ihex_skipfunc skip);

extern HOFF_API void *ihex_ps_getcontext(ihex_parser ps);
extern HOFF_API void ihex_ps_setcontext(ihex_parser ps, void *ctx);

extern int HOFF_API ihex_ps_next(ihex_parser ps, const char **sptr);
/*
 * Incrementally loads the next complete record from the C string
 * pointed to by *sptr.
 *
 * *sptr is updated (that is advanced) accordingly.
 *
 * The next record header is loaded as specified for ihex_rd_next().
 * Then (RECLEN + 1) DATA/INFO and CHKSUM bytes are loaded as if
 * ihex_rd_read() was invoked.  Once completed (possibly incrementally),
 * and the checksum of the now current record has been verified, the
 * record format is checked.  If it is valid, the base address is
 * updated in case of an Extended Address record and finally the value
 * of the RECTYP header field is returned (which is always >= 0 and also
 * accessible via ihex_ps_getrectyp()).  Otherwise, a negative error
 * code is returned at any stage:
 *
 * -IHEX_UNDERFLOW
 *     Stream buffer underflow.  Implies **sptr == '\0'.
 *
 * -IHEX_SYNTAX_ERROR
 *     *sptr points to an illegal input character.
 *
 * Once the record is complete, the values of the various record fields
 * can be accessed:
 *
 *   ihex_ps_getrectyp()   -> RECTYP
 *   ihex_ps_getreclen()   -> RECLEN
 *   ihex_ps_getloadoffs() -> LOADOFFS
 *   ihex_ps_getchksum()   -> CHKSUM
 *   ihex_ps_getdata()     -> (pointer to RECLEN bytes of) DATA
 *   ihex_ps_getinfo()     -> (pointer to RECLEN bytes of) INFO
 *
 * The record's index (see ihex_ps_getindex()) is initialized to 0.
 *
 * The following error codes presuppose a complete record; while the
 * return code is negative to indicate an invalid record, the record's
 * fields may still be accessed as described above (in particular,
 * ihex_ps_getrectyp() is /not/ negative then!).
 *
 * -IHEX_BAD_CHECKSUM
 *     Invalid checksum.  Implies ihex_ps_getsum() != 0.
 *
 * -IHEX_FORMAT_ERROR
 *     Invalid record format.  The record per se may be considered valid
 *     (in terms of the checksum), but one or more of its fields' values
 *     do not conform to specification, for example an illegal RECTYP or
 *     an Extended Segment Address record with a nonzero LOAD OFFSET.
 */

extern HOFF_API void ihex_ps_terminate(ihex_parser ps);

extern HOFF_API unsigned int ihex_ps_getflags(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_setflags(ihex_parser ps,
    unsigned int which, unsigned int value);

extern HOFF_API unsigned long int ihex_ps_getbaseaddr(ihex_parser ps);
extern HOFF_API unsigned long int ihex_ps_getaddr(ihex_parser ps);

extern HOFF_API int ihex_ps_getrectyp(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_getreclen(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_getloadoffs(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_getchksum(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_getsum(ihex_parser ps);
extern HOFF_API const unsigned char *ihex_ps_getdata(ihex_parser ps);
extern HOFF_API const unsigned char *ihex_ps_getinfo(ihex_parser ps);

extern HOFF_API unsigned int ihex_ps_getindex(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_getnremain(ihex_parser ps);
extern HOFF_API unsigned int ihex_ps_getncont(ihex_parser ps);

extern HOFF_API unsigned int ihex_ps_read(void *buf, unsigned int n,
    ihex_parser ps);
/*
 * Reads DATA/INFO bytes from the current Intel HEX record.
 *
 * n or (ihex_ps_getreclen() - ihex_ps_getindex()) content bytes,
 * whichever is less, are copied to buf.  The index (see
 * ihex_ps_getindex()) is incremented accordingly and the number of
 * bytes actually copied is returned.
 *
 * Invoking this function without a current record being available (that
 * is when ihex_ps_getrectyp() < 0) yields undefined behavior.
 */

extern HOFF_API int ihex_ps_readcont(unsigned long int *orgptr, void *block,
    size_t *sizeptr, size_t maxsize, const char **sptr, ihex_parser ps);
/*
 * Incrementally reads contiguous (in terms of address) blocks of DATA
 * across records.
 *
 * This is the most comprehensive of the parser functions and may
 * therefore take a little getting used to.  Its parameters are:
 *
 * orgptr
 *   Pointer to the origin /address/ associated with block, that is the
 *   absolute memory address of the data byte at *block.  Note that even
 *   if *sizeptr is equal to 0, *orgptr must still contain a (in this
 *   case arbitrary) valid address (e.g. 0).
 *
 * block
 *   Pointer to storage for DATA of (at least) maxsize bytes.
 *
 * sizeptr
 *   Pointer to the current size of block, that is the number of /valid/
 *   data bytes.  *sizeptr shall be less than or equal to maxsize.
 *
 * maxsize
 *   Maximum size of block; shall be greater than or equal to *sizeptr.
 *
 * sptr
 *   The Intel HEX stream to read.  *sptr shall point to a C string and
 *   is updated (that is advanced) accordingly.
 *
 * The block, sizeptr and maxsize arguments together specify the storage
 * area for DATA.  At most maxsize - *sizeptr bytes of new data will be
 * read and stored, beginning at block + *sizeptr; *sizeptr will be
 * updated accordingly.
 *
 * The value of *orgptr is used to determine whether the content bytes
 * of the next DATA record are contiguous to the data already stored in
 * block.  Thus whenever *sizeptr is /not/ equal to 0, *orgptr shall
 * hold the absolute memory address of the byte at *block; *orgptr is
 * not modified in this case.  The absolute memory address of the data
 * byte stored at block[i] is *orgptr + i (for 0 <= i < *sizeptr).  If
 * *sizeptr /is/ equal to 0, the absolute memory address of the next
 * DATA byte to be read (that is the one going into *block) is written
 * to *orgptr.  Note that if maxsize is also equal to 0, *orgptr is
 * still updated even though no data is actually stored to block (which
 * must nevertheless be valid!).  This may for instance be used to
 * determine a suitable block argument for a subsequent call that then
 * actually reads the data.
 *
 * ihex_ps_readcont() repeatedly reads records as if ihex_ps_next() was
 * invoked, until either
 *
 *  * ihex_ps_next() returned with a negative error code, which is
 *    passed on to the caller;
 *  * there is no space left in block (*sizeptr >= maxsize), in which
 *    case IHEX_TYP_DAT is returned (even if maxsize == 0!);
 *  * the next DATA byte to be read is not contiguous (in terms of
 *    absolute memory address) to the byte stored at block[*sizeptr - 1]
 *    and IHEX_TYP_DAT is returned, or
 *  * a record of type other than DATA is read.  If *sizeptr == 0, that
 *    record's type is returned, or IHEX_TYP_DAT otherwise.
 *
 * The DATA is read into block as if ihex_ps_read() was invoked.  To
 * "consume" data from block, simply pass a *sizeptr value of 0 on the
 * next invocation.  For record types /other/ than DATA, an explicit
 * call to ihex_ps_terminate() is required to continue; until then, the
 * same record (type) is returned over and over again.
 */

#endif /* IHEX_IHEXPS_H_INCLUDED */
