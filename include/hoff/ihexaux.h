/*
   Copyright (C) 2012, 2014 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#ifndef IHEX_IHEXAUX_H_INCLUDED
#define IHEX_IHEXAUX_H_INCLUDED

#include <hoff/hoffdef.h>

#define IHEX_MKSBA(usba)  ((unsigned long int)((usba) & 0xffffU) << 4)
#define IHEX_MKLBA(ulba)  ((unsigned long int)((ulba) & 0xffffU) << 16)

/*
 * linear: (LBA + DRLO + DRI) MOD 4G
 * segmented: SBA + ([DRLO + DRI] MOD 64K)
 */
#define IHEX_MKADDR(ba, drlo, dri, segmented)  \
	(((ba) \
	  + (((unsigned long int)(drlo) + (dri)) \
	     & (0xffffU | ((unsigned long int)((segmented) != 0) - 1)))) \
	 & 0xffffffffUL)

extern HOFF_API unsigned int ihex_getncont
  (unsigned long int ba, unsigned int drlo, unsigned int dri,
   unsigned int reclen, unsigned int segmented);

#endif /* IHEX_IHEXAUX_H_INCLUDED */
