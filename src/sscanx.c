/*
   Copyright (C) 2012, 2013, 2014, 2015 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include "sscanx.h"

#include <assert.h>

#include <hoff/ihexdef.h>

#include "common.h"

#define ASCII_0  0x30
#define ASCII_9  0x39
#define ASCII_A  0x41
#define ASCII_F  0x46

size_t
hoff_sscanx(void *buf, size_t n, unsigned char *sum, unsigned char *hi,
    const char **sptr, unsigned int flags)
	{
	unsigned char *bs;
	const char *s;
	unsigned int partsum;
	unsigned int mask;
	unsigned int b;

	assert(buf != NULL);
	assert(sum != NULL);
	assert(hi != NULL);
	assert(sptr != NULL);
	assert(*sptr != NULL);

	if (n != 0)
		{
		bs = (unsigned char *)buf;
		s = *sptr;
		partsum = 0;
		/* bit 5 determines case (in ASCII, mind you) */
		HOFF_STATIC_ASSERT(HOFF_F_NOCASE == (1U << 5));
		mask = ~(flags & (1U << 5));
		}
	else
		goto finally;
	if (*hi != 0)
		{
		b = ~*hi;
		*hi = 0;
		goto lo;
		}
	do
		{
		unsigned int lo;

		b = (unsigned int)(unsigned char)*s++ - ASCII_0;
		if (b > 9)
			{
			b = (b & mask) - (ASCII_A - ASCII_0);
			if (b <= 0xf - 0xa)
				b += 0xa;
			else
				goto unget;
			}
		b <<= 4;
 lo:
		lo = (unsigned int)(unsigned char)*s++ - ASCII_0;
		if (lo > 9)
			{
			lo = (lo & mask) - (ASCII_A - ASCII_0);
			if (lo <= 0xf - 0xa)
				lo += 0xa;
			else
				{
				*hi = (unsigned char)~b;
 unget:
				--s;
				break;
				}
			}
		b |= lo;
		*bs++ = (unsigned char)b;
		partsum += b;
		}
		while (--n != 0);
	*sum += partsum;
	*sptr = s;
 finally:
	return n;
	}
