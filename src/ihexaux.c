/*
   Copyright (C) 2012, 2014 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include <hoff/ihexaux.h>

#include "common.h"

unsigned int
ihex_getncont(unsigned long int ba, unsigned int drlo, unsigned int dri,
    unsigned int reclen, unsigned int segmented)
	{
	unsigned long int offs;
	unsigned long int div;
	unsigned int nrem;

	/* NB: no assertions, GIGO! */

	/*
	 * linear: (LBA + DRLO + DRI) MOD 4G
	 * segmented: (DRLO + DRI) MOD 64K
	 */
	offs = drlo + dri;
	if ( !segmented)
		{
		div = UINT32_MAX;
		offs = (ba + offs) & div;
		}
	else
		{
		div = UINT16_MAX;
		offs &= div;
		}
	nrem = reclen - dri;
	return offs <= div - nrem ? nrem : (unsigned int)(div - offs) + 1;
	}
