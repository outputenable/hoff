/*
   Copyright (C) 2012, 2013, 2015 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include "ihexrdi.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "sscanx.h"

static int skip_past_mark(const char **sptr, void *ctx);

int
ihex_rd_init(struct ihex_reader_ *rd)
	{
	assert(rd != NULL);

	ihex_rd_terminate(rd);
	rd->flags = 0;
	rd->skip_past_mark = skip_past_mark;
	rd->ctx = NULL;
	return IHEX_OK;
	}

void
ihex_rd_destroy(struct ihex_reader_ *rd)
	{
	assert(rd != NULL);
	(void)rd;
	}

ihex_reader
ihex_rd_new(void)
	{
	struct ihex_reader_ *rd;

	rd = malloc(sizeof *rd);
	if (rd != NULL && ihex_rd_init(rd) != IHEX_OK)
		{
		free(rd);
		rd = NULL;
		}
	return rd;
	}

void
ihex_rd_free(ihex_reader rd)
	{
	if (rd != NULL)
		{
		ihex_rd_destroy(rd);
		free(rd);
		}
	}

unsigned int
ihex_rd_getflags(ihex_reader rd)
	{
	assert(rd != NULL);
	return rd->flags;
	}

unsigned int
ihex_rd_setflags(ihex_reader rd, unsigned int which, unsigned int value)
	{
	assert(rd != NULL);

	HOFF_STATIC_ASSERT(IHEX_RD_F_NOCASE == HOFF_F_NOCASE);
	which &= IHEX_RD_F_NOCASE;
	value &= which;
	rd->flags = (rd->flags & ~which) | value;
	return rd->flags;
	}

ihex_skipfunc
ihex_rd_getskipfunc(ihex_reader rd)
	{
	assert(rd != NULL);
	return rd->skip_past_mark;
	}

void
ihex_rd_setskipfunc(ihex_reader rd, ihex_skipfunc skip)
	{
	assert(rd != NULL);
	rd->skip_past_mark = skip != NULL ? skip : skip_past_mark;
	}

static int
skip_past_mark(const char **sptr, void *ctx)
	{
	const char *s;
	char c;
	int rc;

	assert(sptr != NULL);
	assert(*sptr != NULL);
	(void)ctx;

	s = *sptr;
	while ((c = *s++) != IHEX_RECORD_MARK)
		{
		if (c != '\0')
			continue;
		--s;
		rc = -IHEX_UNDERFLOW;
		goto finally;
		}
	rc = IHEX_OK;
 finally:
	*sptr = s;
	return rc;
	}

void *
ihex_rd_getcontext(ihex_reader rd)
	{
	assert(rd != NULL);
	return rd->ctx;
	}

void
ihex_rd_setcontext(ihex_reader rd, void *ctx)
	{
	assert(rd != NULL);
	rd->ctx = ctx;
	}

int
ihex_rd_next(ihex_reader rd, const char **sptr)
	{
	int rc;

	assert(rd != NULL);
	assert(sptr != NULL);
	assert(*sptr != NULL);

	if (rd->rectyp >= 0)
		ihex_rd_terminate(rd);
	else if (rd->reclen != 0)
		goto read;
	rc = (*rd->skip_past_mark)(sptr, rd->ctx);
	if (rc == IHEX_OK)
		rd->reclen = IHEX_HDRLEN;  /* counting down */
	else
		goto finally;
 read:
	rd->reclen = (unsigned int)hoff_sscanx
	    (rd->u.hdr + IHEX_HDRLEN - rd->reclen, rd->reclen, &rd->sum,
	     &rd->hi, sptr, rd->flags);
	if (rd->reclen == 0)
		{
		rd->rectyp = rd->u.hdr[3];
		rd->reclen = rd->u.hdr[0];
		rd->u.loadoffs =
		    ((unsigned int)rd->u.hdr[1] << 8) | rd->u.hdr[2];
		rc = rd->rectyp;
		}
	else
		rc = **sptr == '\0' ? -IHEX_UNDERFLOW : -IHEX_SYNTAX_ERROR;
 finally:
	return rc;
	}

void
ihex_rd_terminate(ihex_reader rd)
	{
	assert(rd != NULL);

	rd->rectyp = -1;
	rd->reclen = 0;
	rd->sum = 0;
	rd->hi = 0;
	}

int
ihex_rd_getrectyp(ihex_reader rd)
	{
	assert(rd != NULL);
	return rd->rectyp;
	}

unsigned int
ihex_rd_getreclen(ihex_reader rd)
	{
	assert(rd != NULL);
	return rd->reclen;
	}

unsigned int
ihex_rd_getloadoffs(ihex_reader rd)
	{
	assert(rd != NULL);
	return rd->u.loadoffs;
	}

unsigned int
ihex_rd_getsum(ihex_reader rd)
	{
	assert(rd != NULL);
	return (unsigned int)rd->sum & 0xff;
	}

size_t
ihex_rd_read(void *buf, size_t n, const char **sptr, ihex_reader rd)
	{
	assert(buf != NULL);
	assert(sptr != NULL);
	assert(*sptr != NULL);
	assert(rd != NULL);

	return n - hoff_sscanx(buf, n, &rd->sum, &rd->hi, sptr, rd->flags);
	}
