/*
   Copyright (C) 2012, 2013, 2014, 2015 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include "ihexpsi.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <hoff/ihexaux.h>

#include "common.h"
#include "sscanx.h"

#define TERMINATE(ps)  do \
	{ \
	ps->index = 0; \
	} while (0)

static int nextinfo(struct ihex_parser_ *ps);

int
ihex_ps_init(struct ihex_parser_ *ps)
	{
	assert(ps != NULL);

	ihex_rd_init(&ps->rd);
	ps->baseaddr = 0;
	TERMINATE(ps);
	return IHEX_OK;
	}

void
ihex_ps_destroy(struct ihex_parser_ *ps)
	{
	assert(ps != NULL);
	ihex_rd_destroy(&ps->rd);
	}

ihex_skipfunc
ihex_ps_getskipfunc(ihex_parser ps)
	{
	assert(ps != NULL);
	return ihex_rd_getskipfunc(&ps->rd);
	}

void
ihex_ps_setskipfunc(ihex_parser ps, ihex_skipfunc skip)
	{
	assert(ps != NULL);
	ihex_rd_setskipfunc(&ps->rd, skip);
	}

void *
ihex_ps_getcontext(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->rd.ctx;
	}

void
ihex_ps_setcontext(ihex_parser ps, void *ctx)
	{
	assert(ps != NULL);
	ps->rd.ctx = ctx;
	}

ihex_parser
ihex_ps_new(void)
	{
	struct ihex_parser_ *ps;

	ps = malloc(sizeof *ps);
	if (ps != NULL && ihex_ps_init(ps) != IHEX_OK)
		{
		free(ps);
		ps = NULL;
		}
	return ps;
	}

void
ihex_ps_free(ihex_parser ps)
	{
	if (ps != NULL)
		{
		ihex_ps_destroy(ps);
		free(ps);
		}
	}

int
ihex_ps_next(ihex_parser ps, const char **sptr)
	{
	int rc;

	assert(ps != NULL);
	assert(sptr != NULL);
	assert(*sptr != NULL);

	if (ps->rd.rectyp >= 0)
		/* ihex_rd_next() below takes care of ps->rd */
		TERMINATE(ps);
	else if (ps->index != 0)
		{
		/* fix up rectyp (see below) */
		ps->rd.rectyp += IHEX_RECLEN_MAX + 1;
		goto read;
		}
	rc = ihex_rd_next(&ps->rd, sptr);
	if (rc >= 0)
		{
		/* counting down, incl. CHKSUM */
		ps->index = ps->rd.reclen + 1;
 read:
		ps->index = (unsigned int)hoff_sscanx
		    (ps->buf + ps->rd.reclen + 1 - ps->index, ps->index,
		     &ps->rd.sum, &ps->rd.hi, sptr, ps->rd.flags);
		if (ps->index == 0)
			rc = (ps->rd.sum & 0xff) == 0
			    ? ps->rd.rectyp == IHEX_TYP_DAT ? IHEX_TYP_DAT
			      : nextinfo(ps)
			    : -IHEX_BAD_CHECKSUM;
		else
			{
			/* make sure rectyp is < 0 */
			ps->rd.rectyp -= IHEX_RECLEN_MAX + 1;
			rc = **sptr == '\0' ? -IHEX_UNDERFLOW
			    : -IHEX_SYNTAX_ERROR;
			}
		}
	return rc;
	}

void
ihex_ps_terminate(ihex_parser ps)
	{
	assert(ps != NULL);

	ihex_rd_terminate(&ps->rd);
	TERMINATE(ps);
	}

static int
nextinfo(struct ihex_parser_ *ps)
	{
	int rc;

	assert(ps != NULL);
	assert(ps->rd.rectyp != IHEX_TYP_DAT);

	rc = ihex_ps_checkformat(ps);
	if (rc == IHEX_OK)
		{
		rc = ihex_ps_apply(ps);
		if (rc == IHEX_OK)
			rc = ps->rd.rectyp;
		}
	return rc;
	}

int
ihex_ps_checkformat(ihex_parser ps)
	{
	static const unsigned char speclen[] = { 0, 0, 2, 4, 2, 4 };

	assert(ps != NULL);

	return
	    (ps->rd.rectyp == IHEX_TYP_DAT
	     || (((unsigned int)ps->rd.rectyp
	          < sizeof speclen / sizeof *speclen)
	         && ps->rd.reclen == speclen[ps->rd.rectyp]
	         && ps->rd.u.loadoffs == 0))
	    ? IHEX_OK : -IHEX_FORMAT_ERROR;
	}

int
ihex_ps_apply(ihex_parser ps)
	{
	int rc;

	assert(ps != NULL);

	if (ps->rd.rectyp == IHEX_TYP_ESA || ps->rd.rectyp == IHEX_TYP_ELA)
		{
		unsigned int uba;

		uba = ((unsigned int)ps->buf[0] << 8) | ps->buf[1];
		rc = ps->rd.rectyp == IHEX_TYP_ESA
		    ? ihex_ps_setsegmented(ps, uba)
		    : ihex_ps_setlinear(ps, uba);
		}
	else
		rc = IHEX_OK;
	return rc;
	}

unsigned int
ihex_ps_getflags(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->rd.flags;
	}

unsigned int
ihex_ps_setflags(ihex_parser ps, unsigned int which, unsigned int value)
	{
	assert(ps != NULL);

	HOFF_STATIC_ASSERT(IHEX_PS_F_NOCASE == IHEX_RD_F_NOCASE);
	return ihex_rd_setflags(&ps->rd, which, value);
	}

unsigned long int
ihex_ps_getbaseaddr(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->baseaddr;
	}

int
ihex_ps_setsegmented(ihex_parser ps, unsigned int usba)
	{
	assert(ps != NULL);

	ps->baseaddr = IHEX_MKSBA(usba);
	ps->rd.flags |= IHEX_PS_F_SEGMENTED;
	return IHEX_OK;
	}

int
ihex_ps_setlinear(ihex_parser ps, unsigned int ulba)
	{
	assert(ps != NULL);

	ps->baseaddr = IHEX_MKLBA(ulba);
	ps->rd.flags &= ~IHEX_PS_F_SEGMENTED;
	return IHEX_OK;
	}

unsigned long int
ihex_ps_getaddr(ihex_parser ps)
	{
	assert(ps != NULL);
	return IHEX_MKADDR(ps->baseaddr, ps->rd.u.loadoffs, ps->index,
	    ps->rd.flags & IHEX_PS_F_SEGMENTED);
	}

int
ihex_ps_getrectyp(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->rd.rectyp;
	}

unsigned int
ihex_ps_getreclen(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->rd.reclen;
	}

unsigned int
ihex_ps_getloadoffs(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->rd.u.loadoffs;
	}

unsigned int
ihex_ps_getsum(ihex_parser ps)
	{
	assert(ps != NULL);
	return (unsigned int)ps->rd.sum & 0xff;
	}

unsigned int
ihex_ps_getchksum(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->buf[ps->rd.reclen];
	}

const unsigned char *
ihex_ps_getdata(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->buf;
	}

const unsigned char *
ihex_ps_getinfo(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->buf;
	}

unsigned int
ihex_ps_getindex(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->index;
	}

unsigned int
ihex_ps_getnremain(ihex_parser ps)
	{
	assert(ps != NULL);
	return ps->rd.reclen - ps->index;
	}

unsigned int
ihex_ps_getncont(ihex_parser ps)
	{
	assert(ps != NULL);
	return ihex_getncont(ps->baseaddr, ps->rd.u.loadoffs, ps->index,
	    ps->rd.reclen, ps->rd.flags & IHEX_PS_F_SEGMENTED);
	}

unsigned int
ihex_ps_read(void *buf, unsigned int n, ihex_parser ps)
	{
	unsigned int nrem;

	assert(buf != NULL);
	assert(ps != NULL);

	nrem = ps->rd.reclen - ps->index;
	if (n > nrem)
		n = nrem;
	memcpy(buf, ps->buf + ps->index, n);
	ps->index += n;
	return n;
	}

int
ihex_ps_readcont(unsigned long int *orgptr, void *block, size_t *sizeptr,
  size_t maxsize, const char **sptr, ihex_parser ps)
	{
	size_t size;
	unsigned long int addr;
	int rc;

	assert(orgptr != NULL);
	assert(block != NULL);
	assert(sizeptr != NULL);
	assert(sptr != NULL);
	assert(*sptr != NULL);
	assert(ps != NULL);

	size = *sizeptr;
	if (size <= maxsize)
		{
		unsigned long int maxcont;

		maxcont = UINT32_MAX - *orgptr;
		if (maxsize > maxcont)
			maxsize = (size_t)maxcont + 1;
		addr = (unsigned long int)(*orgptr + size);
		if (ps->rd.rectyp == IHEX_TYP_DAT)
			goto ncont;
		if (ps->rd.rectyp >= 0)
			{
			rc = ps->rd.rectyp;
			goto finally;
			}
		}
	else
		goto dat;
	do
		{
		unsigned int n;
		unsigned long int dataddr;
		size_t nmax;

		for (;;)
			{
			rc = ihex_ps_next(ps, sptr);
			if (rc == IHEX_TYP_DAT)
 ncont:
				{
				n = ihex_ps_getncont(ps);
				if (n != 0)
					break;
				}
			else if (rc >= 0 && size != 0)
				goto dat;
			else
				goto out;
			}
		dataddr = ihex_ps_getaddr(ps);
		if (dataddr != addr)
			{
			if (size == 0)
				{
				unsigned long int maxcont;

				*orgptr = addr = dataddr;
				maxcont = UINT32_MAX - dataddr;
				if (maxsize > maxcont)
					maxsize = (size_t)maxcont + 1;
				}
			else
				break;
			}
		nmax = maxsize - size;
		if (n > nmax)
			n = (unsigned int)nmax;
		memcpy((char *)block + size, ps->buf + ps->index, n);
		ps->index += n;
		addr += n;
		size += n;
		}
		while (size != maxsize);
 dat:
	rc = IHEX_TYP_DAT;
 out:
	*sizeptr = size;
 finally:
	return rc;
	}
