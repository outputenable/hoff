/*
   Copyright (C) 2012, 2014 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#ifndef IHEX_IHEXPSI_H_INCLUDED
#define IHEX_IHEXPSI_H_INCLUDED

#include <hoff/ihexps.h>

#include <hoff/ihexdef.h>

#include "ihexrdi.h"

/* invariant: index <= reclen <= IHEX_RECLEN_MAX < sizeof buf */
struct ihex_parser_
	{
	struct ihex_reader_ rd;

	unsigned int index;
	unsigned long int baseaddr;

	unsigned char buf[IHEX_RECLEN_MAX + 1];  /* one extra for CHKSUM */
	};

extern HOFF_LOCAL int ihex_ps_init(struct ihex_parser_ *ps);
extern HOFF_LOCAL void ihex_ps_destroy(struct ihex_parser_ *ps);

extern HOFF_LOCAL int ihex_ps_checkformat(ihex_parser ps);
extern HOFF_LOCAL int ihex_ps_apply(ihex_parser ps);

extern HOFF_LOCAL int ihex_ps_setsegmented(ihex_parser ps, unsigned int usba);
extern HOFF_LOCAL int ihex_ps_setlinear(ihex_parser ps, unsigned int ulba);

#endif /* IHEX_IHEXPSI_H_INCLUDED */
