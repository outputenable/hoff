/*
   Copyright (C) 2012, 2014, 2015 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include <hoff/ihexstr.h>

#include <stddef.h>

#include "common.h"

/*
 * The technique used below is adapted from code based on suggestions by Bruno
 * Haible and presented in Appendix B Automatic Handler of Arrays of String
 * Pointers of Ulrich Drepper's paper How To Write Shared Libraries
 * (http://www.akkadia.org/drepper/dsohowto.pdf).
 */

#define NELEMS(a)  (sizeof a / sizeof *a)

#define MKNAME(line)  MKNAME1(line)
#define MKNAME1(line)  s##line

#define OTHER_ERROR_STR  "Other error"

static const struct errstrs
	{
#define ERRDEF(n, s)  char MKNAME(__LINE__)[sizeof s];
#include "errstrs.def"
#undef ERRDEF
	char other_error[sizeof OTHER_ERROR_STR];
	}
	errstrs =
	{
#define ERRDEF(n, s)  s,
#include "errstrs.def"
#undef ERRDEF
	OTHER_ERROR_STR
	};

static const unsigned char erroffs[] =
	{
#define ERRDEF(n, s)  offsetof(struct errstrs, MKNAME(__LINE__)),
#include "errstrs.def"
#undef ERRDEF
	offsetof(struct errstrs, other_error)
	};

const char *
ihex_strerror(int errnum)
	{
	if (-(int)NELEMS(erroffs) < errnum && errnum < (int)NELEMS(erroffs))
		{
		if (errnum < 0)
			errnum = -errnum;
		}
	else
		errnum = NELEMS(erroffs) - 1;
	return (const char *)&errstrs + erroffs[errnum];
	}
