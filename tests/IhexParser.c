/*
   Copyright (C) 2012, 2013, 2014 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#if defined _MSC_VER
#define _CRT_SECURE_NO_WARNINGS  /* suppress C4996 */
#endif

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <hoff/ihexps.h>

#include "CuTest.h"

static void
TestInstantiation(CuTest *tc)
	{
	ihex_parser ps;

	ps = ihex_ps_new();
	CuAssertPtrNotNull(tc, ps);
	CuAssertTrue(tc, ihex_ps_getrectyp(ps) < 0);
	CuAssertTrue(tc, ihex_ps_getflags(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getbaseaddr(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getflags(ps) == 0);
	/* other attributes are undefined if rectyp < 0 */
	ihex_ps_free(ps);
	}

static void
TestNext(CuTest *tc)
	{
	ihex_parser ps;
	const char *s;
	const char *snext;
	const unsigned char *data;

	ps = ihex_ps_new();
	CuAssertPtrNotNull(tc, ps);
	
	s = ":02A5A500EEC501\n";

	snext = strchr(s, '\n');
	CuAssertPtrNotNull(tc, snext);
	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_next(ps, &s));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_getrectyp(ps));
	CuAssertTrue(tc, ihex_ps_getreclen(ps) == 2);
	CuAssertTrue(tc, ihex_ps_getloadoffs(ps) == 0xa5a5);
	CuAssertTrue(tc, ihex_ps_getchksum(ps) == 0x01);
	CuAssertTrue(tc, ihex_ps_getsum(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getindex(ps) == 0);
	data = ihex_ps_getdata(ps);
	CuAssertPtrNotNull(tc, data);
	CuAssertTrue(tc, data[0] == 0xee);
	CuAssertTrue(tc, data[1] == 0xc5);

	ihex_ps_free(ps);
	}

static void
TestLinearAddressCalculations(CuTest *tc)
	{
	ihex_parser ps;
	const char *s;
	unsigned char buf[1];
	char *s2;

	ps = ihex_ps_new();
	CuAssertPtrNotNull(tc, ps);

	s = ":010000005AA5\n"
	    ":02000004FFFFFC\n"
	    ":02FFFF000001FF\n";

	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_next(ps, &s));
	CuAssertTrue(tc, ihex_ps_getloadoffs(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getaddr(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getnremain(ps) == 1);
	CuAssertTrue(tc, ihex_ps_getncont(ps) == 1);

	CuAssertIntEquals(tc, IHEX_TYP_ELA, ihex_ps_next(ps, &s));
	CuAssertTrue(tc, ihex_ps_getbaseaddr(ps) == 0xffff0000UL);

	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_next(ps, &s));
	CuAssertTrue(tc, ihex_ps_getloadoffs(ps) == 0xffff);
	CuAssertTrue(tc, ihex_ps_getaddr(ps) == 0xffffffffUL);
	CuAssertTrue(tc, ihex_ps_getnremain(ps) == 2);
	CuAssertTrue(tc, ihex_ps_getncont(ps) == 1);

	CuAssertTrue(tc, ihex_ps_read(buf, 1, ps) == 1);
	CuAssertTrue(tc, *buf == 0);
	CuAssertTrue(tc, ihex_ps_getaddr(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getnremain(ps) == 1);
	CuAssertTrue(tc, ihex_ps_getncont(ps) == 1);

	CuAssertTrue(tc, ihex_ps_read(buf, 1, ps) == 1);
	CuAssertTrue(tc, *buf == 0x1);
	CuAssertTrue(tc, ihex_ps_getaddr(ps) == 0x1);
	CuAssertTrue(tc, ihex_ps_getnremain(ps) == 0);
	CuAssertTrue(tc, ihex_ps_getncont(ps) == 0);

	s = ":FFFF0100000102030405060708090A0B0C0D0E0F"
	             "101112131415161718191A1B1C1D1E1F"
	             "202122232425262728292A2B2C2D2E2F"
	             "303132333435363738393A3B3C3D3E3F"
	             "404142434445464748494A4B4C4D4E4F"
	             "505152535455565758595A5B5C5D5E5F"
	             "606162636465666768696A6B6C6D6E6F"
	             "707172737475767778797A7B7C7D7E7F"
	             "808182838485868788898A8B8C8D8E8F"
	             "909192939495969798999A9B9C9D9E9F"
	             "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
	             "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
	             "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
	             "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
	             "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
	             "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFE80\n";

	s2 = malloc(strlen(s) + 1);
	CuAssertPtrNotNull(tc, s2);
	strcpy(s2, s);

	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_next(ps, &s));
	CuAssertTrue(tc, ihex_ps_getaddr(ps) == 0xffffff01UL);
	CuAssertTrue(tc, ihex_ps_getnremain(ps) == 0xff);
	CuAssertTrue(tc, ihex_ps_getncont(ps) == 0xff);

	/* increase LOAD OFFSET so that address wraps around, adjust CHKSUM */
	++s2[6];
	memcpy(s2 + strlen(s2) - 3, "7F", 2);
	s = s2;

	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_next(ps, &s));
	CuAssertTrue(tc, ihex_ps_getaddr(ps) == 0xffffff02UL);
	CuAssertTrue(tc, ihex_ps_getnremain(ps) == 0xff);
	CuAssertTrue(tc, ihex_ps_getncont(ps) == 0xfe);

	free(s2);
	ihex_ps_free(ps);
	}

static void
TestReadCont(CuTest *tc)
	{
	ihex_parser ps;
	const char *s;
	const char *snext;
	unsigned long int org;
	size_t size;
	unsigned char chunk[4] = { 0 };

	ps = ihex_ps_new();
	CuAssertPtrNotNull(tc, ps);

	s = ":01200000AB34\1\n"
	    ":01200100BC22\2\n"
	    ":01200200CD10\3\n"
	    ":01300000DEF1\4\n";

	snext = strchr(s, '\1');
	org = (unsigned long int)rand();  /* don't care */
	size = 0;
	CuAssertIntEquals(tc, IHEX_TYP_DAT,
	    ihex_ps_readcont(&org, chunk, &size, 0, &s, ps));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertTrue(tc, org == 0x2000);
	CuAssertTrue(tc, size == 0);

	snext = strchr(snext, '\2');
	CuAssertIntEquals(tc, IHEX_TYP_DAT,
	    ihex_ps_readcont(&org, chunk, &size, 2, &s, ps));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertTrue(tc, org == 0x2000);
	CuAssertTrue(tc, size == 2);
	CuAssertTrue(tc, chunk[0] == 0xab);
	CuAssertTrue(tc, chunk[1] == 0xbc);

	snext = strchr(snext, '\4');  /* two records! */
	CuAssertIntEquals(tc, IHEX_TYP_DAT,
	    ihex_ps_readcont(&org, chunk, &size, 4, &s, ps));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertTrue(tc, org == 0x2000);
	CuAssertTrue(tc, size == 3);
	CuAssertTrue(tc, chunk[2] == 0xcd);

	CuAssertIntEquals(tc, IHEX_TYP_DAT, ihex_ps_getrectyp(ps));
	CuAssertTrue(tc, ihex_ps_getreclen(ps) == 1);
	CuAssertTrue(tc, ihex_ps_getloadoffs(ps) == 0x3000);
	CuAssertTrue(tc, *ihex_ps_getdata(ps) == 0xde);

	snext = strchr(snext, '\0');  /* end of string */
	org = 0;
	memset(chunk, 0, sizeof chunk);
	size = 0;
	CuAssertIntEquals(tc, -IHEX_UNDERFLOW,
	    ihex_ps_readcont(&org, chunk, &size, 4, &s, ps));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertTrue(tc, org == 0x3000);
	CuAssertTrue(tc, size == 1);
	CuAssertTrue(tc, chunk[0] == 0xde);

	ihex_ps_free(ps);
	}

static void
TestReadContSkipsLeadingEmptyDataRecords(CuTest *tc)
	{
	ihex_parser ps;
	const char *s;
	unsigned long int org;
	size_t size;
	unsigned char chunk[1];
	int rc;

	ps = ihex_ps_new();
	CuAssertPtrNotNull(tc, ps);

	s = ":00EEEE0024\n"  /* <-- empty record */
	    ":016789000C03\n";

	org = (unsigned long int)rand();
	size = 0;
	memset(chunk, 0, sizeof chunk);
	rc = ihex_ps_readcont(&org, chunk, &size, 0, &s, ps);
	CuAssertIntEquals(tc, IHEX_TYP_DAT, rc);
	CuAssertTrue(tc, org = 0x1234);
	CuAssertTrue(tc, size == 0);
	}

static void
TestReadContSkipsEmptyDataRecords(CuTest *tc)
	{
	ihex_parser ps;
	const char *s;
	unsigned long int org;
	size_t size;
	unsigned char chunk[2];
	int rc;

	ps = ihex_ps_new();
	CuAssertPtrNotNull(tc, ps);

	s = ":011234000AAF\n"
	    ":00EEEE0024\n"  /* <-- empty record */
	    ":011235000BAD\n";

	org = (unsigned long int)rand();
	size = 0;
	memset(chunk, 0, sizeof chunk);
	rc = ihex_ps_readcont(&org, chunk, &size, sizeof chunk, &s, ps);
	CuAssertIntEquals(tc, IHEX_TYP_DAT, rc);
	CuAssertTrue(tc, org == 0x1234);
	CuAssertTrue(tc, size == 2);
	CuAssertTrue(tc, chunk[0] == 0x0a);
	CuAssertTrue(tc, chunk[1] == 0x0b);
	}

CuSuite *
IhexParserGetSuite(void)
	{
	CuSuite *suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, TestInstantiation);
	SUITE_ADD_TEST(suite, TestNext);
	SUITE_ADD_TEST(suite, TestLinearAddressCalculations);
	SUITE_ADD_TEST(suite, TestReadCont);
	SUITE_ADD_TEST(suite, TestReadContSkipsLeadingEmptyDataRecords);
	SUITE_ADD_TEST(suite, TestReadContSkipsEmptyDataRecords);
	return suite;
	}
