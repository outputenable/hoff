/*
   Copyright (C) 2012, 2013 Oliver Ebert

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Except as contained in this notice, the name of an author or copyright
   holder shall not be used in advertising or otherwise to promote the sale,
   use or other dealings in this Software without prior written authorization
   from the author or copyright holder.
*/

#include <stddef.h>
#include <string.h>

#include <hoff/ihexrd.h>

#include "CuTest.h"

static void
TestInstantiation(CuTest *tc)
	{
	ihex_reader rd;

	rd = ihex_rd_new();
	CuAssertPtrNotNull(tc, rd);
	CuAssertTrue(tc, ihex_rd_getrectyp(rd) < 0);
	CuAssertPtrNotNull(tc, ihex_rd_getskipfunc(rd));
	CuAssertPtrEquals(tc, NULL, ihex_rd_getcontext(rd));
	CuAssertTrue(tc, ihex_rd_getflags(rd) == 0);
	/* reclen, loadoffs and (partial) sum are undefined if rectyp < 0 */
	ihex_rd_free(rd);
	}

static int
DummySkipFunc(const char **sptr, void *ctx)
	{
	(void)sptr;
	(void)ctx;
	return 1;
	}

static void
TestSetSkipFunc(CuTest *tc)
	{
	ihex_reader rd;
	ihex_skipfunc skip;

	rd = ihex_rd_new();
	CuAssertPtrNotNull(tc, rd);

	skip = ihex_rd_getskipfunc(rd);
	ihex_rd_setskipfunc(rd, DummySkipFunc);
	CuAssertTrue(tc, DummySkipFunc == ihex_rd_getskipfunc(rd));
	ihex_rd_setskipfunc(rd, NULL);
	CuAssertTrue(tc, skip == ihex_rd_getskipfunc(rd));

	ihex_rd_free(rd);
	}

static void
TestNext(CuTest *tc)
	{
	ihex_reader rd;
	const char *s;
	const char *snext;

	rd = ihex_rd_new();
	CuAssertPtrNotNull(tc, rd);

	s = ":F00BA211 \0h noes!!!\n"
	    "# ... but as long as it doesn't contain a record mark,\r\n"
	    "it's treated as a comment and ignored :D\0EADBEEF\n";

	snext = strchr(s, ' ');
	CuAssertPtrNotNull(tc, snext);
	CuAssertIntEquals(tc, 0x11, ihex_rd_next(rd, &s));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertIntEquals(tc, 0x11, ihex_rd_getrectyp(rd));
	CuAssertTrue(tc, ihex_rd_getreclen(rd) == 0xf0);
	CuAssertTrue(tc, ihex_rd_getloadoffs(rd) == 0xba2);
	CuAssertTrue(tc, ihex_rd_getsum(rd) == 0xae);

	snext = strchr(s, '\0');
	CuAssertPtrNotNull(tc, snext);
	CuAssertIntEquals(tc, -IHEX_UNDERFLOW, ihex_rd_next(rd, &s));
	CuAssertPtrEquals(tc, (char *)snext, (char *)s);
	CuAssertTrue(tc, ihex_rd_getrectyp(rd) < 0);

	snext = strchr(++s, '\0');
	CuAssertPtrNotNull(tc, snext);
	CuAssertIntEquals(tc, -IHEX_UNDERFLOW, ihex_rd_next(rd, &s));
	CuAssertPtrEquals(tc, (void *)snext, (void *)s);
	CuAssertTrue(tc, ihex_rd_getrectyp(rd) < 0);

	snext = strchr(++s, '\n');
	CuAssertPtrNotNull(tc, snext);
	CuAssertIntEquals(tc, 0xef, ihex_rd_next(rd, &s));
	CuAssertPtrEquals(tc, (void *)snext, (void *)s);
	CuAssertIntEquals(tc, 0xef, ihex_rd_getrectyp(rd));
	CuAssertTrue(tc, ihex_rd_getreclen(rd) == 0xde);
	CuAssertTrue(tc, ihex_rd_getloadoffs(rd) == 0xadbe);
	CuAssertTrue(tc, ihex_rd_getsum(rd) == 0x38);

	ihex_rd_free(rd);
	}

CuSuite *
IhexReaderGetSuite(void)
	{
	CuSuite *suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, TestInstantiation);
	SUITE_ADD_TEST(suite, TestSetSkipFunc);
	SUITE_ADD_TEST(suite, TestNext);
	return suite;
	}
